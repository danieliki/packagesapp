export const BACKDATA = [
  {
    title: 'Packages App',
  },
];

export const NAVITEMS = [
  {
    id: 'menu-package',
    name: 'Package',
    route: '/package',
    adminAccess: false,
  },
  {
    id: 'menu-tracking',
    name: 'Tracking',
    route: '/tracking',
    adminAccess: true,
  },
];
