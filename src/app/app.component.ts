import { Component } from '@angular/core';
import { BACKDATA, NAVITEMS } from './app.data';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  appData = BACKDATA[0];
  navItems = NAVITEMS;
  loading = false;
}
