export const CONSTANTS = {
  PACKAGE: {
    STATUS: {
      CLOSED: 1,
      UNLOADED: 2,
      SENT: 3,
      RECEIVED: 4,
    },
  },
};
