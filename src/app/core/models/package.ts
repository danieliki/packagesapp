import { PackageStatus } from './packageStatus';

export class Package {
  Id: number;
  QrCode: number;
  ShippingNumber: number;
  ShippingDate: string;
  Dimensions: string;
  Weight: number;
  CreatedBy: string;
  CreatedOn: string;
  DestinationId: number;
  PackageStatuses: Array<PackageStatus>;
}
