export class PackageStatus {
  Id: number;
  PackageId: number;
  StatusId: number;
  CreatedBy: string;
  CreatedOn: string;

  constructor(StatusId: number, CreatedBy: string, CreatedOn: string, PackageId?: number) {
    this.PackageId = PackageId;
    this.StatusId = StatusId;
    this.CreatedBy = CreatedBy;
    this.CreatedOn = CreatedOn;
  }
}
