export class PackageDestination {
  Id: number;
  PackageId: number;
  DestinationId: number;
  CreatedBy: string;
  CreatedOn: string;
}
