import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UtilsService {
  constructor() {}

  public getNestedProperty(obj: object, path: string) {
    return path.split('.').reduce((o, p) => o && o[p], obj);
  }

  public isNumeric(data: any) {
    return /^\d+$/.test(data);
  }

  public generateQrCode(): number {
    // Generates a random 5 digit number
    return Math.floor(Math.random() * 90000) + 10000;
  }
}
