import { TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';
import { UtilsService } from './utils.service';

describe('GIVEN UtilsService', () => {
  let service: UtilsService;

  const obj = {
    a: 1,
    b: {
      b1: '2',
    },
  };

  configureTestSuite(() => {
    TestBed.configureTestingModule({});
  });

  beforeEach(() => {
    service = TestBed.get(UtilsService);
  });

  it('SHOULD be created', () => {
    expect(service).toBeTruthy();
  });

  it('SHOULD retrieve property referred to an object with nested property', () => {
    const property = 'b.b1';
    const result = service.getNestedProperty(obj, property);

    expect(result).toEqual(obj.b.b1);
  });

  it('SHOULD retrieve property referred to an object with not nested property', () => {
    const property = 'a';
    const result = service.getNestedProperty(obj, property);

    expect(result).toEqual(obj.a);
  });

  it('SHOULD retrieve property referred to an object with not property at all', () => {
    const property = 'b.b2';
    const result = service.getNestedProperty(obj, property);

    expect(result).toBeUndefined();
  });

  it('SHOULD check if property is numeric', () => {
    const property = 'aa45';
    const result = service.isNumeric(property);

    expect(result).toBeFalsy();
  });

  it('SHOULD generate a 5 digit qrCode', () => {
    const result = service.generateQrCode();

    expect(result.toString().length).toBe(5);
  });
});
