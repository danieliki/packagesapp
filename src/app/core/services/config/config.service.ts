import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ConfigService {
  apiUrl = environment.apiUrl;
  endpoints = {
    packages: {
      root: `${this.apiUrl}/packages`,
      byQrCode: `${this.apiUrl}/packages/byQrCode/:qrCode`,
      insertPackageStatus: `${this.apiUrl}/packages/packageStatus`,
    },
    statuses: {
      root: `${this.apiUrl}/statuses`,
    },
    destinations: {
      root: `${this.apiUrl}/destinations`,
    },
  };

  constructor() {}
}
