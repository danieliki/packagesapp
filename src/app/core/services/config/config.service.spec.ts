import { TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';
import { ConfigService } from './config.service';

describe('GIVEN ConfigService', () => {
    configureTestSuite(() => TestBed.configureTestingModule({}));

    it('SHOULD be created', () => {
        const service: ConfigService = TestBed.get(ConfigService);
        expect(service).toBeTruthy();
    });
});
