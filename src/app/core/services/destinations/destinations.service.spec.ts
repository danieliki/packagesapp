import { TestBed } from '@angular/core/testing';
import { DestinationsService } from './destinations.service';
import { configureTestSuite } from 'ng-bullet';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ConfigService } from '../config/config.service';
import { inject } from '@angular/core/testing';
import destinationsData from 'src/assets/mockups/destinations/get-all.json';

describe('GIVEN DestinationsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  const error404Response = {
    body: 'Destinations not found',
    opts: { status: 400, statusText: 'Bad Request' },
  };

  class ConfigServiceStub {
    endpoints = {
      destinations: {
        root: `/destinations`,
      },
    };
  }

  configureTestSuite(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [DestinationsService, { provide: ConfigService, useClass: ConfigServiceStub }],
    })
  );

  afterEach(inject([HttpTestingController], (httpMock: HttpTestingController) => {
    httpMock.verify();
  }));

  it('SHOULD be created', () => {
    const service: DestinationsService = TestBed.get(DestinationsService);
    expect(service).toBeTruthy();
  });

  it('SHOULD call GET service with success status', inject(
    [HttpTestingController, DestinationsService, ConfigService],
    (httpMock: HttpTestingController, destinationsService: DestinationsService, configService: ConfigService) => {
      destinationsService.get().subscribe((data) => {
        expect(data.length).toBe(6);
      });
      const req = httpMock.expectOne(configService.endpoints.destinations.root);
      expect(req.request.method).toEqual('GET');

      req.flush(destinationsData);
    }
  ));

  it('SHOULD call GET service with error status', inject(
    [HttpTestingController, DestinationsService, ConfigService],
    (httpMock: HttpTestingController, destinationsService: DestinationsService, configService: ConfigService) => {
      destinationsService.get().subscribe(
        (data) => {},
        (error) => {
          expect(error.status).toBe(error404Response.opts.status);
          expect(error.error).toBe(error404Response.body);
        }
      );

      const req = httpMock.expectOne(configService.endpoints.destinations.root);
      expect(req.request.method).toEqual('GET');

      req.flush(error404Response.body, error404Response.opts);
    }
  ));
});
