import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config/config.service';
import { Observable } from 'rxjs';
import { Destination } from '../../models/destination';

@Injectable({
  providedIn: 'root',
})
export class DestinationsService {
  constructor(private httpClient: HttpClient, private config: ConfigService) {}

  get(): Observable<Array<Destination>> {
    return this.httpClient.get<Array<Destination>>(this.config.endpoints.destinations.root);
  }
}
