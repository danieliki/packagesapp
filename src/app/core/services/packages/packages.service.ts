import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ConfigService } from '../config/config.service';
import { Package } from '../../models/package';
import { PackageStatus } from '../../models/packageStatus';

@Injectable({
  providedIn: 'root',
})
export class PackagesService {
  constructor(private httpClient: HttpClient, private config: ConfigService) {}

  getByQrCode(qrCode: string): Observable<Package> {
    return this.httpClient.get<Package>(this.config.endpoints.packages.byQrCode.replace(':qrCode', qrCode));
  }

  getPackages(): Observable<Array<Package>> {
    return this.httpClient.get<Array<Package>>(this.config.endpoints.packages.root);
  }

  insertPackage(pack: Package): Observable<any> {
    return this.httpClient.post<any>(this.config.endpoints.packages.root, pack);
  }

  insertPackageStatus(packageStatus: PackageStatus): Observable<any> {
    return this.httpClient.post<any>(this.config.endpoints.packages.insertPackageStatus, packageStatus);
  }
}
