import { TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ConfigService } from '../config/config.service';
import { inject } from '@angular/core/testing';
import { PackagesService } from './packages.service';
import PackageData from '../../../../assets/mockups/packages/get-all.json';
import { Package } from '../../models/package';
import { PackageStatus } from '../../models/PackageStatus';

describe('GIVEN PackagesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  const okResponse = ['Package added correctly'];

  const error404Response = {
    body: 'Package not found',
    opts: { status: 400, statusText: 'Bad Request' },
  };

  const qrCode = '23';
  const pack = new Package();
  const packageStatus = new PackageStatus(1, 'user.name', '');

  class ConfigServiceStub {
    endpoints = {
      packages: {
        root: `/packages`,
        byQrCode: `/packages/byQrCode/:qrCode`,
        insertPackageStatus: `/packages/packageStatus`,
      },
    };
  }

  configureTestSuite(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PackagesService, { provide: ConfigService, useClass: ConfigServiceStub }],
    })
  );

  afterEach(inject([HttpTestingController], (httpMock: HttpTestingController) => {
    httpMock.verify();
  }));

  it('SHOULD be created', () => {
    const service: PackagesService = TestBed.get(PackagesService);
    expect(service).toBeTruthy();
  });

  it('SHOULD call package by qr GET service with success status', inject(
    [HttpTestingController, PackagesService, ConfigService],
    (httpMock: HttpTestingController, packagesService: PackagesService, configService: ConfigService) => {
      packagesService.getByQrCode(qrCode).subscribe((data) => {
        expect(data).toBeTruthy();
      });

      const req = httpMock.expectOne(configService.endpoints.packages.byQrCode.replace(':qrCode', qrCode));
      expect(req.request.method).toEqual('GET');

      req.flush(PackageData);
    }
  ));

  it('SHOULD call package by qr GET service with error status', inject(
    [HttpTestingController, PackagesService, ConfigService],
    (httpMock: HttpTestingController, packagesService: PackagesService, configService: ConfigService) => {
      packagesService.getByQrCode(qrCode).subscribe(
        () => {},
        (error) => {
          expect(error.status).toBe(error404Response.opts.status);
          expect(error.error).toBe(error404Response.body);
        }
      );

      const req = httpMock.expectOne(configService.endpoints.packages.byQrCode.replace(':qrCode', qrCode));
      expect(req.request.method).toEqual('GET');

      req.flush(error404Response.body, error404Response.opts);
    }
  ));

  it('SHOULD insert package with POST service with success status', inject(
    [HttpTestingController, PackagesService, ConfigService],
    (httpMock: HttpTestingController, packagesService: PackagesService, configService: ConfigService) => {
      packagesService.insertPackage(pack).subscribe((data) => {
        expect(data).toBe(okResponse);
      });

      const req = httpMock.expectOne(configService.endpoints.packages.root);
      expect(req.request.method).toEqual('POST');

      req.flush(okResponse);
    }
  ));

  it('SHOULD insert package with POST service with error status', inject(
    [HttpTestingController, PackagesService, ConfigService],
    (httpMock: HttpTestingController, packagesService: PackagesService, configService: ConfigService) => {
      packagesService.insertPackage(pack).subscribe(
        () => {},
        (error) => {
          expect(error.status).toBe(error404Response.opts.status);
          expect(error.error).toBe(error404Response.body);
        }
      );

      const req = httpMock.expectOne(configService.endpoints.packages.root);
      expect(req.request.method).toEqual('POST');

      req.flush(error404Response.body, error404Response.opts);
    }
  ));

  it('SHOULD insert package status with POST service with success status', inject(
    [HttpTestingController, PackagesService, ConfigService],
    (httpMock: HttpTestingController, packagesService: PackagesService, configService: ConfigService) => {
      packagesService.insertPackageStatus(packageStatus).subscribe((data) => {
        expect(data).toBe(okResponse);
      });

      const req = httpMock.expectOne(configService.endpoints.packages.insertPackageStatus);
      expect(req.request.method).toEqual('POST');

      req.flush(okResponse);
    }
  ));

  it('SHOULD insert package status with POST service with error status', inject(
    [HttpTestingController, PackagesService, ConfigService],
    (httpMock: HttpTestingController, packagesService: PackagesService, configService: ConfigService) => {
      packagesService.insertPackageStatus(packageStatus).subscribe(
        () => {},
        (error) => {
          expect(error.status).toBe(error404Response.opts.status);
          expect(error.error).toBe(error404Response.body);
        }
      );

      const req = httpMock.expectOne(configService.endpoints.packages.insertPackageStatus);
      expect(req.request.method).toEqual('POST');

      req.flush(error404Response.body, error404Response.opts);
    }
  ));
});
