import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ConfigService } from '../config/config.service';
import { Status } from '../../models/status';

@Injectable({
  providedIn: 'root',
})
export class StatusesService {
  constructor(private httpClient: HttpClient, private config: ConfigService) {}

  get(): Observable<Array<Status>> {
    return this.httpClient.get<Array<Status>>(this.config.endpoints.statuses.root);
  }
}
