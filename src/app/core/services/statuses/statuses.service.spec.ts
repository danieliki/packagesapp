import { TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ConfigService } from '../config/config.service';
import { inject } from '@angular/core/testing';
import { StatusesService } from './statuses.service';
import statusData from 'src/assets/mockups/statuses/get-all.json';

describe('GIVEN StatusesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  const error404Response = {
    body: 'Status not found',
    opts: { status: 400, statusText: 'Bad Request' },
  };

  class ConfigServiceStub {
    endpoints = {
      statuses: {
        root: `/statuses`,
      },
    };
  }

  configureTestSuite(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [StatusesService, { provide: ConfigService, useClass: ConfigServiceStub }],
    })
  );

  afterEach(inject([HttpTestingController], (httpMock: HttpTestingController) => {
    httpMock.verify();
  }));

  it('SHOULD be created', () => {
    const service: StatusesService = TestBed.get(StatusesService);
    expect(service).toBeTruthy();
  });

  it('SHOULD call GET service with success status', inject(
    [HttpTestingController, StatusesService, ConfigService],
    (httpMock: HttpTestingController, statusesService: StatusesService, configService: ConfigService) => {
      statusesService.get().subscribe((data) => {
        expect(data.length).toBe(4);
      });

      const req = httpMock.expectOne(configService.endpoints.statuses.root);
      expect(req.request.method).toEqual('GET');

      req.flush(statusData);
    }
  ));

  it('SHOULD call GET service with error status', inject(
    [HttpTestingController, StatusesService, ConfigService],
    (httpMock: HttpTestingController, statusesService: StatusesService, configService: ConfigService) => {
      statusesService.get().subscribe(
        (data) => {},
        (error) => {
          expect(error.status).toBe(error404Response.opts.status);
          expect(error.error).toBe(error404Response.body);
        }
      );

      const req = httpMock.expectOne(configService.endpoints.statuses.root);
      expect(req.request.method).toEqual('GET');

      req.flush(error404Response.body, error404Response.opts);
    }
  ));
});
