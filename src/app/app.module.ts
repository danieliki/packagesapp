import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { MatSnackBarModule, MAT_DATE_LOCALE } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DialogMainComponent } from './shared/dialog/dialog.component';
import { DialogModule } from './shared/dialog/dialog.module';
import { LoadingModule } from './shared/loading/loading.module';
import { NavModule } from './shared/nav/nav.module';
import { TopnavModule } from './shared/topnav/topnav.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    MatSnackBarModule,
    DialogModule,
    NavModule,
    TopnavModule,
    LoadingModule,
  ],
  providers: [{ provide: MAT_DATE_LOCALE, useValue: 'es-ES' }],
  entryComponents: [DialogMainComponent],
  bootstrap: [AppComponent],
})
export class AppModule {}
