import { ChangeDetectorRef, Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { finalize } from 'rxjs/internal/operators/finalize';
import { PackagesService } from 'src/app/core/services/packages/packages.service';
import { UtilsService } from 'src/app/core/services/utils/utils.service';
import { rowsAnimation } from 'src/app/shared/animations/template.animations';
import { DialogMainComponent } from 'src/app/shared/dialog/dialog.component';
import { Package } from 'src/app/core/models/package';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  templateUrl: './tracking.component.html',
  styleUrls: ['./tracking.component.scss'],
  animations: [rowsAnimation],
})
export class TrackingComponent implements OnInit, OnDestroy {
  //#region Private Variables
  trackingForm: FormGroup;
  displayedColumns: string[] = [
    'Id',
    'QrCode',
    'Destination',
    'ShippingNumber',
    'ShippingDate',
    'Dimensions',
    'Weight',
    'CreatedBy',
    'CreatedOn',
  ];
  rows: Array<Package> = [];
  loading: boolean;
  dataSource: MatTableDataSource<Package>;
  rowsLength: number;
  filter: string;

  paginator: MatPaginator;
  @ViewChild(MatPaginator, { static: false }) set matPaginator(mp: MatPaginator) {
    this.paginator = mp;
    this.setDataSourceAttributes();
  }

  sort: MatSort;
  @ViewChild(MatSort, { static: false }) set matSort(ms: MatSort) {
    this.sort = ms;
    this.setDataSourceAttributes();
  }

  private ngUnsubscribe: Subject<void> = new Subject<void>();
  //#endregion

  //#region Constructor
  constructor(
    public dialog: MatDialog,
    private packagesService: PackagesService,
    private utilsService: UtilsService,
    private cdRef: ChangeDetectorRef
  ) {}
  //#endregion

  //#region NG Methods
  ngOnInit() {
    this.trackingForm = this.createTrackingFormGroup();
    this.dataSource = new MatTableDataSource();
    this.getPackages();
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
  //#endregion

  //#region Public Methods
  public getPackages() {
    this.loading = true;
    this.trackingForm.get('filterControl').disable();

    this.packagesService
      .getPackages()
      .pipe(
        finalize(() => {
          this.loading = false;
          this.cdRef.detectChanges();
        }),
        takeUntil(this.ngUnsubscribe)
      )
      .subscribe((data: Array<Package>) => {
        if (data.length === 0) {
          this.showDialog('Packages not found');
        }
        this.rows = data;
        this.dataSource = new MatTableDataSource(this.rows);
        this.rowsLength = this.rows.length;

        this.trackingForm.get('filterControl').enable();

        // If there is a word in the filter, remain the filter
        if (this.filter !== undefined) {
          this.trackingForm.controls.filterControl.setValue(this.filter);
          this.applyFilter(this.filter);
        }
      });
  }

  public applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  //#endregion

  //#region Private Methods
  private createTrackingFormGroup() {
    const formBuilder = new FormBuilder();
    return formBuilder.group({
      filterControl: new FormControl(''),
    });
  }

  private setDataSourceAttributes() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sortingDataAccessor = (obj, property) => {
      return this.utilsService.getNestedProperty(obj, property);
    };
    this.dataSource.sort = this.sort;
    this.dataSource.filterPredicate = (data: Package, filter: string): boolean => {
      const matchFilter = [];
      const columns = [
        data.Id.toString(),
        data.QrCode.toString(),
        data.ShippingNumber.toString(),
        data.ShippingDate.toString(),
        data.Dimensions,
        data.Weight.toString(),
        data.CreatedBy,
        data.CreatedOn.toString(),
      ];
      columns.forEach((column) => column && matchFilter.push(column.toLowerCase().includes(filter)));
      return matchFilter.some(Boolean);
    };
  }

  private showDialog(message: string) {
    this.dialog.open(DialogMainComponent, {
      panelClass: 'dialog',
      width: '600px',
      data: {
        header: 'Tracking',
        content: message,
        decline: 1,
      },
    });
  }
  //#endregion
}
