import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { TrackingComponent } from './tracking.component';
import { CommonModule } from '@angular/common';
import { configureTestSuite } from 'ng-bullet';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatCardModule,
  MatButtonModule,
  MatDialogModule,
  MatDatepickerModule,
  MatIconModule,
  MatInputModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule,
} from '@angular/material';
import { LoadingModule } from 'src/app/shared/loading/loading.module';
import { PackagesService } from 'src/app/core/services/packages/packages.service';
import { UtilsService } from 'src/app/core/services/utils/utils.service';
import { of } from 'rxjs';
import packagesData from 'src/assets/mockups/packages/get-all.json';
import noPackagesData from 'src/assets/mockups/empty-data.json';
import { Package } from 'src/app/core/models/package';

fdescribe('GIVEN trackingComponent', () => {
  //#region Private Methods
  class PackagesServiceStub {
    getPackages() {
      return {
        pipe: () => {},
        subscribe: () => {},
      };
    }
  }

  class UtilsServiceStub {
    isNumeric = () => {};
    getNestedProperty = () => {};
  }
  //#endregion

  let component: TrackingComponent;
  let fixture: ComponentFixture<TrackingComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        BrowserAnimationsModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        MatCardModule,
        MatButtonModule,
        MatDialogModule,
        MatDatepickerModule,
        MatIconModule,
        MatInputModule,
        MatNativeDateModule,
        MatPaginatorModule,
        MatSelectModule,
        MatSortModule,
        MatTableModule,
        LoadingModule,
      ],
      declarations: [TrackingComponent],
      providers: [
        { provide: PackagesService, useClass: PackagesServiceStub },
        { provide: UtilsService, useClass: UtilsServiceStub },
      ],
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('SHOULD create', () => {
    expect(component).toBeTruthy();
  });

  it('SHOULD return complete FormBuilder', () => {
    expect(component.trackingForm).toBeDefined();
  });

  describe('WHEN there are packages', () => {
    beforeEach(inject([PackagesService], (packagesService: PackagesService) => {
      spyOn(packagesService, 'getPackages').and.returnValue(of(packagesData));
      fixture.detectChanges();
    }));

    it('SHOULD filter active aircrafts', () => {
      component.applyFilter(packagesData[0].Id.toString());
      expect(packagesData.length === 1);
      component.applyFilter(packagesData[0].QrCode.toString());
      expect(packagesData.length === 1);
      component.applyFilter(packagesData[0].ShippingNumber.toString());
      expect(packagesData.length === 1);
      component.applyFilter(packagesData[0].ShippingDate);
      expect(packagesData.length === 1);
      component.applyFilter(packagesData[0].Dimensions);
      expect(packagesData.length === 1);
      component.applyFilter(packagesData[0].Weight.toString());
      expect(packagesData.length === 1);
      component.applyFilter(packagesData[0].CreatedBy);
      expect(packagesData.length === 1);
      component.applyFilter(packagesData[0].CreatedOn);
      expect(packagesData.length === 1);
    });

    it('SHOULD sort Id column table', () => {
      const sortColumn: HTMLElement = fixture.nativeElement.querySelector('#sortId');
      expect(sortColumn.hasAttribute('mat-sort-header')).toBeTruthy();
    });

    it('SHOULD sort QrCode column table', () => {
      const sortColumn: HTMLElement = fixture.nativeElement.querySelector('#sortQrCode');
      expect(sortColumn.hasAttribute('mat-sort-header')).toBeTruthy();
    });

    it('SHOULD sort Destination column table', () => {
      const sortColumn: HTMLElement = fixture.nativeElement.querySelector('#sortDestination');
      expect(sortColumn.hasAttribute('mat-sort-header')).toBeTruthy();
    });

    it('SHOULD sort ShippingNumber column table', () => {
      const sortColumn: HTMLElement = fixture.nativeElement.querySelector('#sortShippingNumber');
      expect(sortColumn.hasAttribute('mat-sort-header')).toBeTruthy();
    });

    it('SHOULD sort ShippingDate column table', () => {
      const sortColumn: HTMLElement = fixture.nativeElement.querySelector('#sortShippingDate');
      expect(sortColumn.hasAttribute('mat-sort-header')).toBeTruthy();
    });

    it('SHOULD sort Dimensions column table', () => {
      const sortColumn: HTMLElement = fixture.nativeElement.querySelector('#sortDimensions');
      expect(sortColumn.hasAttribute('mat-sort-header')).toBeTruthy();
    });

    it('SHOULD sort Weight column table', () => {
      const sortColumn: HTMLElement = fixture.nativeElement.querySelector('#sortWeight');
      expect(sortColumn.hasAttribute('mat-sort-header')).toBeTruthy();
    });

    it('SHOULD sort CreatedBy column table', () => {
      const sortColumn: HTMLElement = fixture.nativeElement.querySelector('#sortCreatedBy');
      expect(sortColumn.hasAttribute('mat-sort-header')).toBeTruthy();
    });

    it('SHOULD sort CreatedOn column table', () => {
      const sortColumn: HTMLElement = fixture.nativeElement.querySelector('#sortCreatedOn');
      expect(sortColumn.hasAttribute('mat-sort-header')).toBeTruthy();
    });

    describe('WHEN table is paginated', () => {
      it(`SHOULD paginate table IF packages
            is greater then ten and goes through all pages`, fakeAsync(() => {
        fixture.detectChanges();
        tick();

        const leftArrow: HTMLElement = fixture.nativeElement.querySelector('.mat-paginator-navigation-previous');
        const rightArrow: HTMLElement = fixture.nativeElement.querySelector('.mat-paginator-navigation-next');

        expect(component.paginator.pageIndex).toBe(0);
        expect(leftArrow.getAttribute('ng-reflect-disabled')).toBe('true');
        expect(rightArrow.getAttribute('ng-reflect-disabled')).toBe('false');

        rightArrow.click();
        fixture.detectChanges();
        tick();
        rightArrow.click();
        fixture.detectChanges();
        tick();

        expect(component.paginator.pageIndex).toBe(2);
        expect(leftArrow.getAttribute('ng-reflect-disabled')).toBe('false');
        expect(rightArrow.getAttribute('ng-reflect-disabled')).toBe('false');

        rightArrow.click();
        fixture.detectChanges();
        tick();

        expect(component.paginator.pageIndex).toBe(3);
        expect(leftArrow.getAttribute('ng-reflect-disabled')).toBe('false');
        expect(rightArrow.getAttribute('ng-reflect-disabled')).toBe('false');
      }));
    });
  });

  describe('WHEN there are no packages', () => {
    beforeEach(inject([PackagesService], (packagesService: PackagesService) => {
      spyOn(packagesService, 'getPackages').and.returnValue(of(noPackagesData));
      fixture.detectChanges();
    }));

    it('SHOULD go to new package button is shown', () => {
      const btnAddNewPackage = fixture.nativeElement.querySelector('.btn-redirect');
      expect(btnAddNewPackage).toBeTruthy();
    });
  });
});
