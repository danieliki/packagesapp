import { CommonModule } from '@angular/common';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';
import { PackageDialogComponent } from './package-dialog.component';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { MatDialogModule, MatDialogRef } from '@angular/material';

describe('GIVEN PackageDialogComponent', () => {
  let component: PackageDialogComponent;
  let fixture: ComponentFixture<PackageDialogComponent>;

  class MatDialogRefStub {
    close() {}
  }

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [CommonModule, MatDialogModule, ZXingScannerModule],
      declarations: [PackageDialogComponent],
      providers: [{ provide: MatDialogRef, useClass: MatDialogRefStub }],
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PackageDialogComponent);
    component = fixture.componentInstance;
  });

  it('SHOULD create', () => {
    expect(component).toBeTruthy();
  });
});
