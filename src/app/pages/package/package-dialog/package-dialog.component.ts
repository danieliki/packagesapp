import { Component, OnDestroy, OnInit } from "@angular/core";
import { MatDialogRef, MatDialog } from "@angular/material";
import { Subject } from "rxjs";
import { DialogMainComponent } from "src/app/shared/dialog/dialog.component";
import { BarcodeFormat } from "@zxing/library";

@Component({
  selector: "app-package-dialog",
  templateUrl: "./package-dialog.component.html",
  styleUrls: ["./package-dialog.component.scss"],
})
export class PackageDialogComponent implements OnInit, OnDestroy {
  //#region Variables
  formatsEnabled: BarcodeFormat[] = [BarcodeFormat.QR_CODE];
  qrResultString: string;
  dialog: MatDialog;
  public loading: boolean;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  //#endregion

  //#region Constructor
  constructor(public dialogRef: MatDialogRef<DialogMainComponent>) {}
  //#endregion

  //#region NG Methods
  clearResult(): void {
    this.qrResultString = null;
  }

  onCodeResult(resultString: string) {
    this.qrResultString = resultString;
    this.dialogRef.close(resultString);
  }

  ngOnInit(): void {
    this.loading = true;
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
  //#endregion
}
