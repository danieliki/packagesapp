import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MatDialogModule } from "@angular/material";
import { PackageDialogComponent } from "./package-dialog.component";
import { ZXingScannerModule } from "@zxing/ngx-scanner";

const components = [PackageDialogComponent];

@NgModule({
  declarations: [...components],
  imports: [CommonModule, MatDialogModule, ZXingScannerModule],
  exports: [...components],
})
export class PackageDialogModule {}
