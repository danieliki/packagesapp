import { CommonModule } from '@angular/common';
import { ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';
import { PackageComponent } from './package.component';
import { RouterTestingModule } from '@angular/router/testing';
import { PackagesService } from 'src/app/core/services/packages/packages.service';
import { FormsModule, ReactiveFormsModule, FormGroup } from '@angular/forms';
import {
  MatButtonModule,
  MatDialogModule,
  MatIconModule,
  MatInputModule,
  MatCardModule,
  MatDialog,
} from '@angular/material';
import { LoadingModule } from 'src/app/shared/loading/loading.module';
import { Package } from 'src/app/core/models/package';
import { of } from 'rxjs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UtilsService } from 'src/app/core/services/utils/utils.service';
import { StatusesService } from 'src/app/core/services/statuses/statuses.service';
import { Status } from 'src/app/core/models/Status';
import { PackageStatus } from 'src/app/core/models/PackageStatus';
import { DestinationsService } from 'src/app/core/services/destinations/destinations.service';
import { Destination } from 'src/app/core/models/Destination';
import { PackageStatusDestinationModule } from './package-status-destination/package-status-destination.module';
import { PackageDialogModule } from './package-dialog/package-dialog.module';
import { PackageDetailsModule } from './package-details/package-details.module';
import PackageUnloaded from 'src/assets/mockups/packages/get-unloaded-package.json';
import PackageSent from 'src/assets/mockups/packages/get-sent-package.json';

describe('GIVEN PackageComponent', () => {
  let component: PackageComponent;
  let fixture: ComponentFixture<PackageComponent>;
  const packageStatus: PackageStatus = new PackageStatus(2, 'user.name', 'Fri Apr 17 2020');
  const pack: Package = new Package();
  pack.Id = 1;
  pack.PackageStatuses = [packageStatus];
  const statuses: Array<Status> = [
    {
      Id: 1,
      Name: 'Closed',
    },
    {
      Id: 2,
      Name: 'Unloaded',
    },
    {
      Id: 3,
      Name: 'Sent',
    },
    {
      Id: 4,
      Name: 'Received',
    },
  ];

  const Destinations: Array<Destination> = [
    {
      Id: 1,
      Name: 'Madrid',
    },
    {
      Id: 2,
      Name: 'Barcelona',
    },
    {
      Id: 3,
      Name: 'Paris',
    },
    {
      Id: 4,
      Name: 'Berlin',
    },
  ];

  class MatDialogStub {
    open = {
      afterClosed: () => of('OK'),
    };
  }

  class PackagesServiceStub {
    getByQrCode() {
      return {
        pipe: () => {},
        subscribe: () => {},
      };
    }

    insertPackage() {
      return {
        pipe: () => {},
        subscribe: () => {},
      };
    }

    insertPackageStatuses() {
      return {
        pipe: () => {},
        subscribe: () => {},
      };
    }
  }

  class StatusesServiceStub {
    get() {
      return {
        subscribe: () => {},
      };
    }
  }

  class UtilsServiceStub {
    isNumeric = () => {};
  }

  class DestinationsServiceStub {
    get() {
      return {
        pipe: () => {},
        subscribe: () => {},
      };
    }
  }

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        BrowserAnimationsModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        MatCardModule,
        MatButtonModule,
        MatDialogModule,
        MatIconModule,
        MatInputModule,
        LoadingModule,
        PackageDialogModule,
        PackageStatusDestinationModule,
        PackageDetailsModule,
      ],
      declarations: [PackageComponent],
      providers: [
        { provide: MatDialog, useClass: MatDialogStub },
        { provide: PackagesService, useClass: PackagesServiceStub },
        { provide: StatusesService, useClass: StatusesServiceStub },
        { provide: UtilsService, useClass: UtilsServiceStub },
        { provide: DestinationsService, useClass: DestinationsServiceStub },
      ],
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PackageComponent);
    component = fixture.componentInstance;
    component.package = new Package();
    component.packageStatusDestinationForm = new FormGroup({});
    component.packageDetailsForm = new FormGroup({});
    fixture.detectChanges();
  });

  it('SHOULD create', () => {
    expect(component).toBeTruthy();
  });

  describe('WHEN scan QR button is clicked', () => {
    describe('AND dialog return nothing', () => {
      it('SHOULD show message dialog', inject(
        [UtilsService, PackagesService],
        (utilsService: UtilsService, packagesService: PackagesService) => {
          spyOn(utilsService, 'isNumeric').and.returnValue(false);
          spyOn(packagesService, 'getByQrCode').and.returnValue(of(null));

          const ebuttonScanQr = fixture.nativeElement.querySelector('.buttonScanQr');
          ebuttonScanQr.click();
          fixture.detectChanges();

          expect(utilsService.isNumeric).not.toHaveBeenCalled();
          expect(packagesService.getByQrCode).not.toHaveBeenCalled();
        }
      ));
    });

    describe('AND dialog return not numeric QR data', () => {
      it('SHOULD show message dialog', inject(
        [UtilsService, PackagesService],
        (utilsService: UtilsService, packagesService: PackagesService) => {
          spyOn(utilsService, 'isNumeric').and.returnValue(false);
          spyOn(packagesService, 'getByQrCode').and.returnValue(of(null));

          const ebuttonScanQr = fixture.nativeElement.querySelector('.buttonScanQr');
          ebuttonScanQr.click();
          fixture.detectChanges();

          expect(packagesService.getByQrCode).not.toHaveBeenCalled();
        }
      ));
    });

    describe('AND dialog return numeric QR data', () => {
      describe('AND Package not exists', () => {
        beforeEach(inject(
          [StatusesService, DestinationsService, PackagesService, UtilsService],
          (
            statusesService: StatusesService,
            destinationsService: DestinationsService,
            packagesService: PackagesService,
            utilsService: UtilsService
          ) => {
            spyOn(utilsService, 'isNumeric').and.returnValue(true);
            spyOn(statusesService, 'get').and.returnValue(of(statuses));
            spyOn(destinationsService, 'get').and.returnValue(of(Destinations));
            spyOn(packagesService, 'getByQrCode').and.returnValue(of(null));

            const ebuttonScanQr = fixture.nativeElement.querySelector('.buttonScanQr');
            ebuttonScanQr.click();
            fixture.detectChanges();
          }
        ));

        it('SHOULD set statuses value', () => {
          expect(component.statuses).toEqual(statuses);
        });

        it('SHOULD set Destinations value', () => {
          expect(component.destinations).toEqual(Destinations);
        });

        it('SHOULD call getByQrCode', inject([PackagesService], (packagesService: PackagesService) => {
          expect(packagesService.getByQrCode).toHaveBeenCalled();
        }));

        it('SHOULD show dialog', inject([MatDialog], (dialog: MatDialog) => {
          expect(dialog.open).toHaveBeenCalled();
        }));

        it('SHOULD set loading to false', () => {
          expect(component.loading).toBeFalsy();
        });

        describe('AND status is closed', () => {
          beforeEach(() => {
            component.packageStatusDestinationForm.value.statusesControl = statuses[0];
            fixture.detectChanges();
          });

          describe('WHEN Save button is clicked', () => {
            beforeEach(inject(
              [UtilsService, PackagesService],
              (utilsService: UtilsService, packagesService: PackagesService) => {
                spyOn(packagesService, 'insertPackage').and.returnValue(of(null));

                component.packageStatusDestinationForm.value.statusesControl = statuses[1];
                fixture.detectChanges();

                component.savePackage();
              }
            ));

            it('SHOULD insert package', inject([PackagesService], (packagesService: PackagesService) => {
              expect(packagesService.insertPackage).toHaveBeenCalled();
            }));

            it('SHOULD open dialog', inject([MatDialog], (dialog: MatDialog) => {
              expect(dialog.open).toHaveBeenCalled();
            }));

            it('SHOULD set package QR to null', () => {
              expect(component.package.QrCode).toBeUndefined();
            });
          });
        });
      });

      describe('AND Package exists', () => {
        beforeEach(inject(
          [StatusesService, DestinationsService, PackagesService, UtilsService],
          (
            statusesService: StatusesService,
            destinationsService: DestinationsService,
            packagesService: PackagesService,
            utilsService: UtilsService
          ) => {
            spyOn(utilsService, 'isNumeric').and.returnValue(true);
            spyOn(statusesService, 'get').and.returnValue(of(statuses));
            spyOn(destinationsService, 'get').and.returnValue(of(Destinations));
            spyOn(packagesService, 'getByQrCode').and.returnValue(of(pack));

            const ebuttonScanQr = fixture.nativeElement.querySelector('.buttonScanQr');
            ebuttonScanQr.click();
            fixture.detectChanges();
          }
        ));

        it('SHOULD set statuses value', () => {
          expect(component.statuses).toEqual(statuses);
        });

        it('SHOULD set Destinations value', () => {
          expect(component.destinations).toEqual(Destinations);
        });

        it('SHOULD call getByQrCode', inject([PackagesService], (packagesService: PackagesService) => {
          expect(packagesService.getByQrCode).toHaveBeenCalled();
        }));

        it('SHOULD open dialog', inject([MatDialog], (dialog: MatDialog) => {
          expect(dialog.open).toHaveBeenCalled();
        }));

        describe('AND status is closed', () => {
          beforeEach(() => {
            component.packageStatusDestinationForm.value.statusesControl = statuses[0];
            fixture.detectChanges();
          });

          describe('WHEN Save button is clicked', () => {
            beforeEach(inject([PackagesService], (packagesService: PackagesService) => {
              spyOn(packagesService, 'insertPackageStatus').and.returnValue(of(null));

              component.savePackage();
            }));

            it('SHOULD insert package status', inject([PackagesService], (packagesService: PackagesService) => {
              expect(packagesService.insertPackageStatus).toHaveBeenCalled();
            }));

            it('SHOULD open dialog', inject([MatDialog], (dialog: MatDialog) => {
              expect(dialog.open).toHaveBeenCalled();
            }));

            it('SHOULD set package QR to null', () => {
              expect(component.package.QrCode).toBeUndefined();
            });
          });
        });
      });
    });
  });
});
