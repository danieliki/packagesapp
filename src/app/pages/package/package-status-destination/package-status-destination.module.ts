import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule, MatSelectModule, MatCheckboxModule } from '@angular/material';
import { PackageStatusDestinationComponent } from '../package-status-Destination/package-status-Destination.component';

@NgModule({
  declarations: [PackageStatusDestinationComponent],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, MatInputModule, MatSelectModule, MatCheckboxModule],
  exports: [PackageStatusDestinationComponent],
})
export class PackageStatusDestinationModule {}
