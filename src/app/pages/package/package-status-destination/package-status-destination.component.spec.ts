import { ComponentFixture, TestBed, fakeAsync, inject } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';
import { ReactiveFormsModule, FormsModule, FormControl, FormGroup } from '@angular/forms';
import { MatSelectModule, MatInputModule, MatCheckboxModule, MatDialog } from '@angular/material';
import { PackageStatusDestinationComponent } from './package-status-destination.component';
import { Status } from 'src/app/core/models/status';
import { Destination } from 'src/app/core/models/destination';
import { PackageStatus } from 'src/app/core/models/PackageStatus';
import { PackageDestination } from 'src/app/core/models/packageDestination';
import { CommonModule } from '@angular/common';
import { Package } from 'src/app/core/models/package';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';
import { UtilsService } from 'src/app/core/services/utils/utils.service';
import { SimpleChange } from '@angular/core';
import { StatusesService } from 'src/app/core/services/statuses/statuses.service';
import { PackagesService } from 'src/app/core/services/packages/packages.service';
import { of } from 'rxjs';

describe('GIVEN PackageStatusDestinationComponent', () => {
  let component: PackageStatusDestinationComponent;
  let fixture: ComponentFixture<PackageStatusDestinationComponent>;
  const status = new Status();
  (status.Id = 1), (status.Name = 'Closed');
  const secondStatus = new Status();
  (secondStatus.Id = 1), (secondStatus.Name = 'Unloaded');
  const destination = new Destination();
  (destination.Id = 1), (destination.Name = 'CDG');
  const packageStatus: PackageStatus = new PackageStatus(1, 'user.name', 'Fri Apr 17 2020');
  const newPackageStatus: PackageStatus = new PackageStatus(2, 'user.name', 'Fri Apr 17 2020');
  const packageDestination = new PackageDestination();
  packageDestination.DestinationId = 1;
  let eStatusesControl: any;
  let eDestinationsControl: any;

  class UtilsServiceStub {
    checkShowConditions = () => {};
    checkIfPackageHasNotThisStatus = () => {};
    isUnloadedStatus = () => {};
    isSentStatus = () => {};
    isUndefinedStatus = () => {};
  }

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatInputModule,
        MatSelectModule,
        MatCheckboxModule,
        BrowserAnimationsModule,
      ],
      declarations: [PackageStatusDestinationComponent],
      providers: [{ provide: UtilsService, useClass: UtilsServiceStub }],
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PackageStatusDestinationComponent);
    component = fixture.componentInstance;
    component.destinations = new Array<Destination>();
    component.destinations.push(destination);
    component.statuses = new Array<Status>();
    component.statuses.push(status);
    component.package = new Package();
    component.package.PackageStatuses = new Array<PackageStatus>();
    component.package.PackageStatuses = [packageStatus, newPackageStatus];
    component.package.DestinationId = packageDestination.Id;
    eStatusesControl = fixture.debugElement.query(By.css('#statusesControl'));
    eDestinationsControl = fixture.debugElement.query(By.css('#destinationsControl'));
    component.selectedStatus = status;
    component.selectedDestination = destination;
    fixture.detectChanges();
  });

  it('SHOULD create', () => {
    expect(component).toBeTruthy();
  });

  it('SHOULD return the complete FormBuilder', () => {
    expect(component.packageStatusDestinationForm).toBeDefined();
    expect(component.packageStatusDestinationForm.get('statusesControl')).toBeDefined();
    expect(component.packageStatusDestinationForm.get('destinationsControl')).toBeDefined();
  });

  describe('WHEN AfterViewChecked', () => {
    describe('AND checkDisableDestinationControl is true', () => {
      describe('AND disableControl is true', () => {
        it('SHOULD disable DestinationsControl', () => {
          fixture.detectChanges();

          component.ngAfterViewChecked();

          expect(component.packageStatusDestinationForm.controls.destinationsControl.disabled).toBeTruthy();
        });
      });

      describe('OR package has value', () => {
        describe('AND status is Unloaded', () => {
          describe('AND destinationsControl has value', () => {
            describe('AND statusesControl value has not changed', () => {
              it('SHOULD disable DestinationsControl', () => {
                component.packageStatusDestinationForm.controls.statusesControl.setValue(2);
                component.packageStatusDestinationForm.controls.destinationsControl.setValue(2);
                component.packageStatusDestinationForm.value.statusesControl.StatusId ===
                  component.package.PackageStatuses[component.package.PackageStatuses.length - 1].StatusId;
                fixture.detectChanges();

                component.ngAfterViewChecked();

                expect(component.packageStatusDestinationForm.controls.destinationsControl.disabled).toBeTruthy();
              });
            });
          });
        });
      });
    });

    describe('AND checkDisableDestinationControl is false', () => {
      it('SHOULD keep DestinationsControl enabled', () => {
        fixture.detectChanges();

        component.ngAfterViewChecked();

        expect(component.packageStatusDestinationForm.controls.destinationsControl.disabled).toBeFalsy();
      });
    });
  });

  describe('WHEN statusesControl is changed', () => {
    describe('AND Package not exists', () => {
      describe('AND status is unloaded', () => {
        beforeEach(fakeAsync(async () => {
          await fixture.whenStable().then(() => {
            eStatusesControl.triggerEventHandler('selectionChange', {
              value: { StatusId: 2 },
            });
            fixture.detectChanges();
          });
        }));

        it('SHOULD activate destination validations', () => {
          expect(component.packageStatusDestinationForm.controls.statusesControl.validator).toBeTruthy();
        });

        it('SHOULD show destinations control', () => {
          expect(eDestinationsControl.nativeElement.hasAttribute('disabled')).toBeFalsy();
        });
      });

      describe('AND status is other', () => {
        beforeEach(fakeAsync(async () => {
          await fixture.whenStable().then(() => {
            eStatusesControl.triggerEventHandler('selectionChange', {
              value: { StatusId: 1 },
            });
            fixture.detectChanges();
          });
        }));

        it('SHOULD set selectedDestination to undefined', () => {
          expect(component.selectedDestination).toBeUndefined();
        });

        it('SHOULD remove destination control value', () => {
          expect(eDestinationsControl.value).toBeUndefined();
        });
      });
    });

    describe('AND Package exists', () => {
      describe('AND PackageStatues not contains status unloaded', () => {
        describe('AND selected status is not unloaded status', () => {
          beforeEach(fakeAsync(
            inject([UtilsService], (utilsService: UtilsServiceStub) => {
              component.package.Id = 1;
              component.selectedStatus = status;
              eStatusesControl.triggerEventHandler('selectionChange', {
                value: { StatusId: 1 },
              });
              fixture.detectChanges();
            })
          ));

          it('SHOULD set destinations control default value to empty', () => {
            expect(component.packageStatusDestinationForm.controls.destinationsControl.value).toBe('');
          });
        });
      });

      describe('AND PackageStatues contains status unloaded', () => {
        beforeEach(fakeAsync(
          inject([UtilsService], (utilsService: UtilsServiceStub) => {
            component.package.Id = 1;
            component.selectedStatus = status;
            eStatusesControl.triggerEventHandler('selectionChange', {
              value: { StatusId: 1 },
            });
            fixture.detectChanges();
          })
        ));

        it('SHOULD keep destinations control value', () => {
          expect(component.packageStatusDestinationForm.controls.destinationsControl.value).toBe(destination);
        });
      });

      describe('AND selected status is unloaded', () => {
        beforeEach(fakeAsync(
          inject([UtilsService], (utilsService: UtilsServiceStub) => {
            component.package.Id = 1;
            component.selectedStatus = secondStatus;
            eStatusesControl.triggerEventHandler('selectionChange', {
              value: { StatusId: 2 },
            });
            fixture.detectChanges();
          })
        ));

        it('SHOULD keep destinations control value', () => {
          expect(component.packageStatusDestinationForm.controls.destinationsControl.value).toBe(destination);
        });
      });
    });
  });

  describe('WHEN destinationsControl is changed', () => {
    it('SHOULD set status value to null', () => {
      eDestinationsControl.triggerEventHandler('selectionChange', {
        value: { StatusId: 3 },
      });
      fixture.detectChanges();
    });

    it('SHOULD emit packageStatusDestinationForm', () => {
      spyOn(component.packageStatusDestinationChange, 'emit');

      eDestinationsControl.triggerEventHandler('selectionChange', {
        value: { StatusId: 3 },
      });
      fixture.detectChanges();

      expect(component.packageStatusDestinationChange.emit).toHaveBeenCalledWith(
        component.packageStatusDestinationForm
      );
    });
  });
});
