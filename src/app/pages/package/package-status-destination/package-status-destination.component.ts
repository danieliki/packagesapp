import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  AfterViewChecked,
  ChangeDetectorRef,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { Status } from 'src/app/core/models/status';
import { Destination } from 'src/app/core/models/destination';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { Package } from 'src/app/core/models/package';

@Component({
  selector: 'app-package-status-destination',
  templateUrl: './package-status-destination.component.html',
  styleUrls: ['./package-status-destination.component.scss', '../../../core/styles/package.scss'],
})
export class PackageStatusDestinationComponent implements OnInit, AfterViewChecked {
  @Input() package: Package;
  @Input() statuses: Array<Status>;
  @Input() destinations: Array<Destination>;
  @Output() packageStatusDestinationChange: EventEmitter<any> = new EventEmitter<any>();

  //#region Private Variables
  packageStatusDestinationForm: FormGroup;
  selectedStatus: Status;
  selectedDestination: Destination;
  //#endregion

  //#region Constructor
  constructor(private cdRef: ChangeDetectorRef) {}
  //#endregion

  //#region NG Methods
  ngOnInit(): void {
    this.setFormControlsDefaultValues();
    this.packageStatusDestinationForm = this.createPackageStatusDestinationFormGroup();
    this.packageStatusDestinationChange.emit(this.packageStatusDestinationForm);
  }

  ngAfterViewChecked(): void {
    if (this.package.Id) {
      this.packageStatusDestinationForm.controls.destinationControl.disable();
      this.cdRef.detectChanges();
    }
  }
  //#endregion

  //#region Public Methods

  //#endregion

  //#region Private Methods
  private createPackageStatusDestinationFormGroup(): FormGroup {
    const formBuilder = new FormBuilder();
    return formBuilder.group({
      statusesControl: new FormControl(this.selectedStatus || '', [Validators.required]),
      destinationControl: new FormControl(this.selectedDestination || ''),
    });
  }

  private setFormControlsDefaultValues() {
    if (this.package.PackageStatuses && this.package.PackageStatuses.length > 0) {
      this.selectedStatus = this.statuses.find(
        (s) => s.Id === this.package.PackageStatuses[this.package.PackageStatuses.length - 1].Id
      );
    }

    if (this.package.DestinationId) {
      this.selectedDestination = this.destinations.find((d) => d.Id === this.package.DestinationId);
    }
  }
  //#endregion
}
