import { Component, OnInit, ChangeDetectorRef, AfterViewChecked, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material';
import { PackagesService } from 'src/app/core/services/packages/packages.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { finalize, takeUntil } from 'rxjs/operators';
import { Package } from 'src/app/core/models/package';
import { DialogMainComponent } from 'src/app/shared/dialog/dialog.component';
import { Status } from 'src/app/core/models/Status';
import { UtilsService } from 'src/app/core/services/utils/utils.service';
import { PackageStatus } from 'src/app/core/models/PackageStatus';
import { Destination } from 'src/app/core/models/destination';
import { StatusesService } from 'src/app/core/services/statuses/statuses.service';
import { DestinationsService } from 'src/app/core/services/destinations/destinations.service';
import { Subject } from 'rxjs';
import { PackageDialogComponent } from './package-dialog/package-dialog.component';

@Component({
  selector: 'app-package',
  templateUrl: './package.component.html',
  styleUrls: ['../../core/styles/package.scss', './package.component.scss'],
})
export class PackageComponent implements OnInit, OnDestroy, AfterViewChecked {
  //#region Private Variables
  user = 'yo.com';
  dialogWidth = '600px';
  dialogHeader = 'Package';
  dialogErrorHeader = 'Oops, an error has ocurred';
  loading = false;
  statuses: Array<Status>;
  destinations: Array<Destination>;
  package: Package;
  isNewPackage = false;
  packageForm: FormGroup;
  packageStatusDestinationForm: FormGroup;
  packageDetailsForm: FormGroup;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  //#endregion

  //#region Constructor
  constructor(
    public dialog: MatDialog,
    private packagesService: PackagesService,
    private statusesService: StatusesService,
    private destinationsService: DestinationsService,
    private utilsService: UtilsService,
    private cdRef: ChangeDetectorRef
  ) {}
  //#endregion

  ngOnInit(): void {
    this.package = new Package();
    this.packageForm = new FormBuilder().group({});
    this.packageStatusDestinationForm = new FormBuilder().group({});
    this.packageDetailsForm = new FormBuilder().group({});
    this.loading = true;
    this.getDestinations();
    this.getStatuses();
    this.loading = false;
  }

  ngAfterViewChecked(): void {
    this.cdRef.detectChanges();
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  //#region Public Methods
  public savePackage() {
    this.loading = true;
    const existsPackageInDB = this.package.Id;

    if (!existsPackageInDB) {
      this.insertPackage();
    } else {
      const packageStatus = this.createPackageStatus();
      this.insertPackageStatus(packageStatus);
    }
  }

  public openDialog() {
    const dialogTitle = `Scan QR`;
    const dialogSubtitle = `Please, scan QR`;
    const dialogRef = this.dialog.open(PackageDialogComponent, {
      width: '800px',
      data: {
        header: { dialogTitle, dialogSubtitle },
        body: {},
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result !== undefined) {
        const isNumericQr = this.utilsService.isNumeric(result);

        if (isNumericQr) {
          this.getPackageByQrCode(result);
        } else {
          this.showDialog(this.dialogErrorHeader, 'QR not valid');
        }
      }
    });
  }

  public createNewPackage() {
    this.isNewPackage = true;
  }
  //#endregion

  //#region Private Methods
  private getPackageByQrCode(qrCode: any) {
    this.loading = true;
    this.package = new Package();

    this.packagesService
      .getByQrCode(qrCode)
      .pipe(
        finalize(() => {
          this.loading = false;
        }),
        takeUntil(this.ngUnsubscribe)
      )
      .subscribe((data: Package) => {
        this.package = data;

        if (data !== null) {
          this.showDialog(this.dialogHeader, 'QR found');
        } else {
          this.showDialog(this.dialogHeader, 'QR not found');
        }
      });
  }

  private getStatuses() {
    this.statusesService
      .get()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((data: Array<Status>) => {
        this.statuses = data;
      });
  }

  private getDestinations() {
    this.destinationsService
      .get()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((data: Array<Destination>) => {
        this.destinations = data;
      });
  }

  private showDialog(header: string, message: string) {
    this.dialog.open(DialogMainComponent, {
      width: this.dialogWidth,
      data: {
        header: header,
        content: message,
        decline: 1,
      },
    });
  }

  private createPackage() {
    this.package.QrCode = this.utilsService.generateQrCode();
    this.package.ShippingNumber = this.packageDetailsForm.value.shippingNumberControl;
    this.package.ShippingDate = this.packageDetailsForm.value.shippingDateControl.toDateString();
    this.package.Dimensions = this.packageDetailsForm.value.dimensionsControl;
    this.package.Weight = this.packageDetailsForm.value.weightControl;
    this.package.CreatedBy = this.user;
    this.package.CreatedOn = new Date().toDateString();
    this.package.PackageStatuses = [this.createPackageStatus(null)];
    this.package.DestinationId = this.packageStatusDestinationForm.value.destinationControl.Id;

    return this.package;
  }

  private createPackageStatus(packageId?: number): PackageStatus {
    return new PackageStatus(
      this.packageStatusDestinationForm.value.statusesControl.Id,
      this.user,
      new Date().toDateString(),
      packageId
    );
  }

  private insertPackage() {
    this.package = this.createPackage();

    this.packagesService
      .insertPackage(this.package)
      .pipe(
        finalize(() => {
          this.loading = false;
        }),
        takeUntil(this.ngUnsubscribe)
      )
      .subscribe(
        (data: string) => {
          this.showDialog(this.dialogHeader, data);
          this.package = new Package();
        },
        () => {}
      );
  }

  private insertPackageStatus(packageStatus: PackageStatus) {
    this.packagesService
      .insertPackageStatus(packageStatus)
      .pipe(
        finalize(() => {
          this.doFinalizeActions();
        }),
        takeUntil(this.ngUnsubscribe)
      )
      .subscribe(
        (data: string) => {
          this.showDialog(this.dialogHeader, data);
          this.package.QrCode = null;
        },
        () => {}
      );
  }

  private doFinalizeActions(): void {
    this.package = new Package();
    this.loading = false;
  }
  //#endregion
}
