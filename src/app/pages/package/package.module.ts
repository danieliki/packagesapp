import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatCardModule, MatDialogModule, MatIconModule, MatInputModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { LoadingModule } from 'src/app/shared/loading/loading.module';
import { PackageComponent } from './package.component';
import { PackageDialogModule } from './package-dialog/package-dialog.module';
import { PackageDialogComponent } from './package-dialog/package-dialog.component';
import { PackageStatusDestinationModule } from './package-status-destination/package-status-destination.module';
import { PackageDetailsComponent } from './package-details/package-details.component';
import { PackageDetailsModule } from './package-details/package-details.module';

const routes = [
  {
    path: '',
    component: PackageComponent,
  },
];

@NgModule({
  declarations: [PackageComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatButtonModule,
    MatDialogModule,
    MatIconModule,
    MatInputModule,
    LoadingModule,
    PackageDialogModule,
    PackageStatusDestinationModule,
    PackageDetailsModule,
  ],
  entryComponents: [PackageDialogComponent],
})
export class PackageModule {}
