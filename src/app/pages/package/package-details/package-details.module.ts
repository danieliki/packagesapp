import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule, MatIconModule, MatInputModule, MatNativeDateModule } from '@angular/material';
import { PackageDetailsComponent } from './package-details.component';

@NgModule({
  declarations: [PackageDetailsComponent],
  imports: [CommonModule, ReactiveFormsModule, MatDatepickerModule, MatInputModule, MatNativeDateModule, MatIconModule],
  exports: [PackageDetailsComponent],
})
export class PackageDetailsModule {}
