import { ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';
import { PackageDetailsComponent } from './package-details.component';
import { configureTestSuite } from 'ng-bullet';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule, MatInputModule, MatNativeDateModule, MatIconModule } from '@angular/material';
import { Package } from 'src/app/core/models/package';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';
import { SimpleChange } from '@angular/core';

describe('GIVEN_PackageDetailComponent', () => {
  let component: PackageDetailsComponent;
  let fixture: ComponentFixture<PackageDetailsComponent>;
  let eshippingNumberControl: any;
  const shippingNumberControlValue = '12345';

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        ReactiveFormsModule,
        MatDatepickerModule,
        MatInputModule,
        MatNativeDateModule,
        MatIconModule,
        BrowserAnimationsModule,
      ],
      declarations: [PackageDetailsComponent],
    });
  });

  describe('WHEN disableControls is true', () => {
    beforeEach(() => {
      fixture = TestBed.createComponent(PackageDetailsComponent);
      component = fixture.componentInstance;
      const pack: Package = new Package();
      component.package = pack;
      fixture.detectChanges();
    });

    it('SHOULD create', () => {
      expect(component).toBeTruthy();
    });

    it('SHOULD disable form controls', () => {
      expect(component.packageDetailsForm.controls.shippingNumberControl.disabled).toBeTruthy();
      expect(component.packageDetailsForm.controls.shippingDateControl.disabled).toBeTruthy();
      expect(component.packageDetailsForm.controls.quantityControl.disabled).toBeTruthy();
      expect(component.packageDetailsForm.controls.kgsControl.disabled).toBeTruthy();
    });
  });

  describe('WHEN disableControls is false', () => {
    beforeEach(() => {
      fixture = TestBed.createComponent(PackageDetailsComponent);
      component = fixture.componentInstance;
      const pack: Package = new Package();
      component.package = pack;
      fixture.detectChanges();
    });

    it('SHOULD enable form controls', () => {
      expect(component.packageDetailsForm.controls.shippingNumberControl.disabled).toBeFalsy();
      expect(component.packageDetailsForm.controls.shippingDateControl.disabled).toBeFalsy();
      expect(component.packageDetailsForm.controls.quantityControl.disabled).toBeFalsy();
      expect(component.packageDetailsForm.controls.kgsControl.disabled).toBeFalsy();
    });
  });

  describe('WHEN onNgChanges is called', () => {
    beforeEach(() => {
      fixture = TestBed.createComponent(PackageDetailsComponent);
      component = fixture.componentInstance;
      const pack: Package = new Package();
      component.package = pack;
      fixture.detectChanges();
    });

    it('SHOULD enable FormControls', () => {
      component.packageDetailsForm = null;
      fixture.detectChanges();

      expect(component.packageDetailsForm.controls.shippingNumberControl.disabled).toBeFalsy();
      expect(component.packageDetailsForm.controls.shippingDateControl.disabled).toBeFalsy();
      expect(component.packageDetailsForm.controls.quantityControl.disabled).toBeFalsy();
      expect(component.packageDetailsForm.controls.kgsControl.disabled).toBeFalsy();
    });
  });

  describe('WHEN a PackageDetail value is changed', () => {
    beforeEach(() => {
      fixture = TestBed.createComponent(PackageDetailsComponent);
      component = fixture.componentInstance;
      const pack: Package = new Package();
      component.package = pack;
      fixture.detectChanges();
    });

    it('SHOULD emit packageDetailsForm', () => {
      spyOn(component.packageDetailsChange, 'emit');

      eshippingNumberControl = fixture.debugElement.query(By.css('#shippingNumberControl'));
      component.packageDetailsForm.get('shippingNumberControl').setValue(shippingNumberControlValue);
      eshippingNumberControl.nativeElement.dispatchEvent(new Event('input'));
      fixture.detectChanges();

      expect(component.packageDetailsChange.emit).toHaveBeenCalledWith(component.packageDetailsForm);
      expect(component.packageDetailsForm.controls.shippingNumberControl.value).toEqual(shippingNumberControlValue);
    });
  });
});
