import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Package } from 'src/app/core/models/package';

@Component({
  selector: 'app-package-details',
  templateUrl: './package-details.component.html',
  styleUrls: ['../../../core/styles/package.scss'],
})
export class PackageDetailsComponent implements OnInit {
  @Input() package: Package;
  @Output() packageDetailsChange: EventEmitter<any> = new EventEmitter<any>();

  //#region Private Variables
  packageDetailsForm: FormGroup;
  shippingDateOpened: any;
  //#endregion

  //#region Constructor
  constructor() {}
  //#endregion

  //#region NG Methods
  ngOnInit(): void {
    this.packageDetailsForm = this.createPackageDetailsFormGroup();
    this.disableFormControls();
    this.packageDetailsChange.emit(this.packageDetailsForm);
  }
  //#endregion

  //#region Public Methods
  public onFormChange() {
    this.packageDetailsChange.emit(this.packageDetailsForm);
  }
  //#endregion

  //#region Private Methods
  private createPackageDetailsFormGroup(): FormGroup {
    const formBuilder = new FormBuilder();
    const pack = this.package || new Package();

    return formBuilder.group({
      shippingNumberControl: new FormControl(pack.ShippingNumber, [Validators.required, Validators.pattern(/^\d+$/)]),
      shippingDateControl: new FormControl(new Date(pack.ShippingDate), [Validators.required]),
      dimensionsControl: new FormControl(pack.Dimensions, [Validators.required]),
      weightControl: new FormControl(pack.Weight, [Validators.required, Validators.pattern(/^\d+$/)]),
    });
  }

  private disableFormControls() {
    if (this.package) {
      this.packageDetailsForm.get('shippingDateControl').disable();
      this.packageDetailsForm.get('shippingNumberControl').disable();
      this.packageDetailsForm.get('weightControl').disable();
      this.packageDetailsForm.get('dimensionsControl').disable();
    } else {
      this.packageDetailsForm.get('shippingDateControl').enable();
      this.packageDetailsForm.get('shippingNumberControl').enable();
      this.packageDetailsForm.get('weightControl').enable();
      this.packageDetailsForm.get('dimensionsControl').enable();
    }
  }
  //#endregion
}
