import { CommonModule } from '@angular/common';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { configureTestSuite } from 'ng-bullet';
import { MainPageComponent } from './main-page.component';

describe('GIVEN MainPageComponent', () => {
    let component: MainPageComponent;
    let fixture: ComponentFixture<MainPageComponent>;

    configureTestSuite(() =>
        TestBed.configureTestingModule({
            declarations: [MainPageComponent],
            imports: [CommonModule, RouterTestingModule]
        })
    );

    beforeEach(() => {
        fixture = TestBed.createComponent(MainPageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('SHOULD create', () => {
        expect(component).toBeTruthy();
    });
});
