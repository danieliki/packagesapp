import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MainPageComponent } from './main-page.component';

const routes = [
    {
        path: '',
        component: MainPageComponent
    }
];

@NgModule({
    declarations: [MainPageComponent],
    imports: [CommonModule, RouterModule.forChild(routes)]
})
export class MainPageModule {}
