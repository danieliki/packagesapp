import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    loadChildren: "./pages/main-page/main-page.module#MainPageModule",
  },
  {
    path: "package",
    loadChildren: "./pages/package/package.module#PackageModule",
  },
  {
    path: "tracking",
    loadChildren: "./pages/tracking/tracking.module#TrackingModule",
  },

  // otherwise redirect to home
  { path: "**", redirectTo: "" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
