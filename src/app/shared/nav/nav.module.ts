import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule, MatMenuModule, MatToolbarModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { NavComponent } from './nav.component';

const components = [NavComponent];

@NgModule({
    declarations: [...components],
    imports: [CommonModule, RouterModule, MatToolbarModule, MatMenuModule, MatButtonModule],
    exports: [...components]
})
export class NavModule {}
