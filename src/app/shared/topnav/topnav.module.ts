import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule, MatMenuModule, MatToolbarModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { TopnavComponent } from './topnav.component';

const components = [TopnavComponent];

@NgModule({
    declarations: [...components],
    imports: [CommonModule, RouterModule, MatToolbarModule, MatMenuModule, MatButtonModule],
    exports: [...components]
})
export class TopnavModule {}
