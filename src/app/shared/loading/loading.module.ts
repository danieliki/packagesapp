import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatProgressSpinnerModule } from '@angular/material';
import { LoadingComponent } from './loading.component';

const components = [LoadingComponent];

@NgModule({
    declarations: [...components],
    imports: [CommonModule, MatProgressSpinnerModule],
    exports: [...components]
})
export class LoadingModule {}
