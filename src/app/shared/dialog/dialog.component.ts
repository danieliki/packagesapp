import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

export interface DialogData {
  header: string;
  content: string;
}

@Component({
  selector: "app-dialog-overview",
  templateUrl: "./dialog.component.html",
  styleUrls: ["./dialog.component.scss"],
})
export class DialogMainComponent {
  constructor(
    public dialogRef: MatDialogRef<DialogMainComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}
}
