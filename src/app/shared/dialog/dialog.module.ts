import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MatButtonModule, MatDialogModule } from "@angular/material";
import { DialogMainComponent } from "./dialog.component";

const components = [DialogMainComponent];

@NgModule({
  declarations: [...components],
  imports: [CommonModule, MatDialogModule, MatButtonModule],
  exports: [...components],
  entryComponents: [DialogMainComponent],
})
export class DialogModule {}
